﻿using MachineLearningCSharpImageML.Model;
using System;

namespace MachineLearningCSharpImage
{
    class Program
    {
        static void Main(string[] args)
        {
            // Add input data
            var input = new ModelInput();
            Console.WriteLine("Input the flower picture:");
            input.ImageSource = @"..\..\..\TestingImages\test.jpg";
            // Load model and predict output of sample data
            ModelOutput result = ConsumeModel.Predict(input);
            Console.WriteLine($"Text: {input.ImageSource}\nWhat kind of flower is it? Roses or Violet? : {result.Prediction}");
            Console.WriteLine("Roses % =" + result.Score[0] * 100);
            Console.WriteLine("Violet % =" + result.Score[1] * 100);
        }
    }
}
